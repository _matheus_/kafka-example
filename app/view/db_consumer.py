from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import os
from app.db.schemas import Base, Mensagem
from app.infra.kafka_handler import KafkaConsumer


def run():
  engine = create_engine('sqlite:////home/leandro/Documents/Facily/kafka-example/master/db/mensagens.db')
  Base.metadata.create_all(engine)
  session = sessionmaker(bind=engine)()

  topic = os.environ['CLOUDKARAFKA_TOPIC_PREFIX'] + os.environ['CLOUDKARAFKA_TOPIC']

  consumer_broker = KafkaConsumer('db_consumer')

  print('DB CONSUMER')

  try:
    while True:
      msg = consumer_broker.consume(topic)

      if msg != '':
        session.add(Mensagem(texto=msg))
        session.commit()

        print('Mensagem recebida e armazenada:', msg)
  except KeyboardInterrupt:
    print("\nPrograma interrompido pelo usuário.")

