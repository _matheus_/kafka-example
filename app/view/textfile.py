from app.infra.kafka_handler import KafkaConsumer
import os

def adicionar_ao_arquivo(texto):
  with open('/home/leandro/Documents/Facily/kafka-example/master/db/textfile.txt', 'a') as f:
    f.writelines(texto)


def run():
  topic = os.environ['CLOUDKARAFKA_TOPIC_PREFIX'] + os.environ['CLOUDKARAFKA_TOPIC']

  consumer_broker = KafkaConsumer('textfile')

  print('TEXTFILE CONSUMER')

  try:
    while True:
      msg = consumer_broker.consume(topic)

      if msg != '':
        print('Mensagem recebida e adicionada ao arquivo:', msg)

        adicionar_ao_arquivo(msg)
  except KeyboardInterrupt:
    print("\nPrograma interrompido pelo usuário.")
