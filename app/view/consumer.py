from app.infra.kafka_handler import KafkaConsumer
import os


def run():
  topic = os.environ['CLOUDKARAFKA_TOPIC_PREFIX'] + os.environ['CLOUDKARAFKA_TOPIC']

  consumer_broker = KafkaConsumer('consumer')

  print('CONSUMER')

  try:
    while True:
      msg = consumer_broker.consume(topic)

      if msg != '':
        print('Mensagem recebida:', msg)
  except KeyboardInterrupt:
    print("\nPrograma interrompido pelo usuário.")
