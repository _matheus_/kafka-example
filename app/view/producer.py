from app.infra.kafka_handler import KafkaPublisher
import os

def run():
  topic = os.environ['CLOUDKARAFKA_TOPIC_PREFIX'] + os.environ['CLOUDKARAFKA_TOPIC']

  producer_broker = KafkaPublisher()

  try:
    while True:
      producer_broker.publish(message=input('Mensagem: '), topic=topic)
  except KeyboardInterrupt:
    print("\nPrograma interrompido pelo usuário.")