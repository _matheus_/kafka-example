from sqlalchemy import Column, Integer, String, DateTime, Sequence
from sqlalchemy.orm import declarative_base
from datetime import datetime

Base = declarative_base()

class Mensagem(Base):
  __tablename__ = 'mensagens'

  id = Column(Integer, Sequence('mensagem_id_seq'), primary_key=True)
  texto = Column(String)
  data_recebimento = Column(DateTime, default=datetime.now())