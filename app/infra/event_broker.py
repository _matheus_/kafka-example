from abc import ABC, abstractmethod

class AbstractPublisher(ABC):
  @abstractmethod
  def publish(self, message, topic):
    pass


class AbstractConsumer(ABC):
  @abstractmethod
  def consume(self, topic):
    pass
