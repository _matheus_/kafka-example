from .event_broker import AbstractPublisher, AbstractConsumer
from confluent_kafka import Producer, Consumer
from dotenv import load_dotenv
import os

load_dotenv()

producer_conf = {
    'bootstrap.servers': os.environ['CLOUDKARAFKA_BROKERS'],
    'security.protocol': 'SASL_SSL',
    'sasl.mechanisms': 'SCRAM-SHA-256',
    'sasl.username': os.environ['CLOUDKARAFKA_USERNAME'],
    'sasl.password': os.environ['CLOUDKARAFKA_PASSWORD']
}

consumer_conf = {
    'session.timeout.ms': 6000,
    'default.topic.config': {'auto.offset.reset': 'smallest'},
}
consumer_conf.update(producer_conf)


class KafkaPublisher(AbstractPublisher):
  def __init__(self):
    self.__producer = Producer(**producer_conf)


  def publish(self, message, topic):
    self.__producer.produce(topic, message)
    self.__producer.flush()


class KafkaConsumer(AbstractConsumer):
  def __init__(self, group):
    consumer_conf['group.id'] = group
    self.__consumer = Consumer(**consumer_conf)


  def consume(self, topic):
    self.__consumer.subscribe([topic])

    msg = self.__consumer.poll(timeout=1)

    return str(msg.value()) if msg is not None else ''
