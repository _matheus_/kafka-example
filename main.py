from app.view import consumer, producer, db_consumer, textfile
import sys

def main():
  args = sys.argv[1:]
  targets = {
    'produce': producer.run,
    'consume': consumer.run,
    'db_consume': db_consumer.run,
    'textfile': textfile.run
  }
  str_targets = " | ".join(targets.keys())

  if not args:
    print(f'Informe o tipo de inicialização que quer para o sistema: ({str_targets})')
    exit(-1)
  
  if args[0] not in targets:
    print(f'Tipo de inicialização inválido! Opções válidas: {str_targets}')
    exit(-2)
  
  targets[args[0]]()



if __name__ == '__main__':
  main()
